Create a python environment using anaconda
    conda create -n spinningup python=3.6
    conda activate spinningup

Install stable baselines3
    pip install stable-baselines3
