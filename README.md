# ran-simulator
How to use RAN Simulator?
1. Setup the Project
2. Train the Model
3. Copy trained Model into : ran-simulator\src\simulation
4. Evaluate the model and the baselines
5. Copy evaluated simulation results \
	RR results : ran-simulator\src\simulation\final_results\<results_name>\rr \
	PF results : ran-simulator\src\simulation\final_results\<results_name>\pf \
	MCQI results : ran-simulator\src\simulation\final_results\<results_name>\mcqi \
	RL results : ran-simulator\src\simulation\final_results\<results_name>\rl
6. Plot boxplots for each algorithm

----------------------------------------------------------------------------------------------
- Project Setup 

Set three directory as source \
a. ran-simulator \
b. rl-baselines3-zoo \
c. stable-baselines3

Interpreter Setup \
follow instructions on interpreter_setup.md

----------------------------------------------------------------------------------------------
- Configure Simulation

a. Training Hyperparameters
rl-baselines3-zoo\hyperparams\a2c.yml:  in the beginning of the file \
b. Simulation parameters
ran-simulator\src\simulation\simparam.py

----------------------------------------------------------------------------------------------
- Training the Model

Script:  rl-baselines3-zoo\train_ransim.py: 

Run Configurations: \
	Script path: ..\rl-baselines3-zoo\train_ransim.py \
	Parameters: --algo a2c --env ransim-v0 --env-kwargs t_final:1000

To Do: \
	run the script.

Output: \
	Simulation Results: rl-baselines3-zoo\results \
	Trained Model:rl-baselines3-zoo\logs\a2c\ransim-v0_#\best_model

----------------------------------------------------------------------------------------------
- Evaluating the model and the Baselines

Script:  ran-simulator\src\simulation\Test_Sim_variable_distance.py \
Results Directory: ran-simulator\src\simulation\results

configuration: \
	line 11, t_final: duration of simulation time frame in ms \
	line 14, round_count: the number of simulation time frames

To Do: \
	run the script.

Output: \
	Simulation Results: ran-simulator\src\simulation\results

----------------------------------------------------------------------------------------------
- Plotting

Script:  ran-simulator\src\simulation\plot_variable_distance.py \
Results Directory: ran-simulator\src\simulation\results

configuration: \
	line 18, c_algo: {rr,pf,mcqi or rl} controller algorithm results to be plotted \
	line 19, parent_dir: final_results\<results_name> parent directory for the results

To Do: \
	For each controller algorithm, set configuration and run the script.